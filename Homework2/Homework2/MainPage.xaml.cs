﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Homework2
{
    public partial class MainPage : ContentPage
    {
        string first;//first number and the answer
        string second;//second number
        string opreator;// + - * /
        bool error;//error for divide 0
        bool takingFirstNumber;// take first number or second number
        bool retakeFirstNumber;// after = re take first number
        bool secondNumberReady;// second number ready

        public MainPage()
        {
            InitializeComponent();
            first = "";
            second = "";
            opreator = "";
            takingFirstNumber = true;
            retakeFirstNumber = true;
            secondNumberReady = false;
            error = false;
        }

        void Handle_NumberClicked(object sender, System.EventArgs e)
        {
            if (error)
                return;
            Button btn = sender as Button;
            string str = btn.Text.ToString();
            if(!takingFirstNumber)// taking second number
            {
                if (second.Length == 0)// have opreator and have first number, so it is start second number
                {
                    second = str;
                    secondNumberReady = true;//ready for opreator calculate
                    Output.Text = str;
                }
                else// taking second number
                {
                    second += str;
                    if (!Output.Text.Equals("0"))
                    {
                        Output.Text += str;
                    }
                }
            }
            else //taking the first number
            {
                if (first.Length == 0)
                {
                    first = str;
                    Output.Text = str;
                }
                else if (retakeFirstNumber) // did it after =
                {
                    first = str;
                    Output.Text = str;
                    retakeFirstNumber = false;
                }
                else// taking first number after first digit
                {
                    first += str;
                    if (!Output.Text.Equals("0"))
                    {
                        Output.Text += str;
                    }
                    else
                    {
                        Output.Text += str;
                    }
                }
            }
        }

        void Handle_OpreatorClicked(object sender, System.EventArgs e)
        {
            if (error)
                return;
            Button btn = sender as Button;
            string str = btn.Text.ToString();
            if(str.Contains("="))// calculate
            {
                if(first.Length==0)// no input just equal
                {
                    Output.Text = "0";
                }
                else if(takingFirstNumber)// no opreator
                {
                    Output.Text = first;
                }
                else 
                {
                    if (second.Length==0)// didn't input second number using first number be the second number
                        second = first;
                    if(double.Parse(second).Equals(0)&&opreator.Contains("/"))
                    {
                        error = true;
                        Output.Text = "ERROR";
                        return;
                    }
                    Calculate();
                    takingFirstNumber = true;
                    Output.Text = first;
                }
            }
            else if(first.Length==0)// didn't input first number let first number be 0
            {
                first = "0";
                opreator = str;
                takingFirstNumber = false;// start to take second number
                Output.Text += str;
            }
            else if(takingFirstNumber)// assign opreator
            {
                opreator = str;
                takingFirstNumber = false;// start to take second number
                Output.Text += str;
            }
            else if(!takingFirstNumber&&secondNumberReady)//using opreator as equal
            {
                if (double.Parse(second).Equals(0) && opreator.Contains("/"))
                {
                    error = true;
                    Output.Text = "ERROR";
                    return;
                }
                Calculate();
                opreator = str;
                second = "";
                Output.Text = first + str;
                secondNumberReady = false;
            }
            else if (!takingFirstNumber)// opreator change
            {
                opreator = str;
                secondNumberReady = false;
                Output.Text = Output.Text.Substring(0, Output.Text.Length - 1) + str;
            }

        }
        

        void Handle_ClearClicked(object sender, System.EventArgs e)
        {
            error = false;
            first = "";
            second = "";
            opreator = "";
            takingFirstNumber = true;
            secondNumberReady = false;
            Output.Text = "0";
        }


        private void Calculate()// put answer to first 
        {
            double answer=0,a,b;
            a = double.Parse(first);
            b = double.Parse(second);
            if (opreator.Contains("+"))
            { answer = a + b; }
            else if (opreator.Contains("-"))
            { answer = a - b; }
            else if (opreator.Contains("X"))
            { answer = a * b; }
            else if (opreator.Contains("/"))
            { answer = a / b; }

            first = answer.ToString();
            second = "";
            retakeFirstNumber = true;//ready to take another round
        }
    }
}

